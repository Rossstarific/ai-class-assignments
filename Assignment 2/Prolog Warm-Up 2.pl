/*prime( N ) :-
    divisor_set( N, [1, N] ).
divisor_set( N, DivSet ) :-
    setof( K,
           ( in_range( K, 1, N ),
             N mod K =:= 0 ),
           DivSet ).
in_range( K, K, High ) :-
    K =< High.
in_range( K, Low, High ) :-
    Low < High,
    Low1 is Low + 1,
    in_range( K, Low1, High ).*/
%a more efficient version
prime( 2 ).
prime( N ) :-
    N mod 2 =:= 1,
    divisor_set( N,[1, N] ).
divisor_set(N, DivSet) :-
    setof( K,
           ( in_range2( K, 1, N ),
             N mod K =:= 0 ),
           DivSet ).
in_range2( K, K, High ) :-
    K =< High.
in_range2( K, Low, High ) :-
    Low < High,
    Low1 is Low + 2,
    in_range2( K, Low1, High ).


