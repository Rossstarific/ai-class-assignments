likes( jim, sandra ).
likes( jim, bill).
likes( peggy, sam ).
likes( bill, sandra ).

drinks( jim, beer ).
drinks( peggy, coke ).
drinks( bill, juice ).
drinks( sam, gin ).

smokes( bill ).
smokes( sandra ).
smokes( sam ).

will_dance( jim, peggy ) :-
    drinks( peggy, coke ).
will_dance( bill, sandra ) :-
    likes( bill, sandra ),
    drinks( sandra, beer ).
will_dance( sam, peggy ) :-
    likes( peggy, sam ),
    drinks( sam, gin ),
    % does not smoke
    \+ smokes( peggy ).
