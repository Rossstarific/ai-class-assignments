% (a) [text after % is a comment]
avoid( Person, swimming_pools_in_winter) :-
    catch_colds( Person ).
/* (b) [a bracketed comment] */
catch_colds( Person ) :-
    no_hat_in_winter( Person ).
%(c)
no_hat_in_winter( Person ) :-
    no_hat_at_all( Person ).
%(d)
no_hat_at_all( jacky ).

