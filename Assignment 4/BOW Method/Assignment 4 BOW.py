import nltk
import os
from nltk.tag import pos_tag
from nltk.tokenize import RegexpTokenizer
from nltk.probability import FreqDist
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import PunktSentenceTokenizer
from urllib import request
from nltk.probability import FreqDist
import numpy
from sklearn.feature_extraction.text import CountVectorizer
import string


BOWfile = open("BOWData.txt", "w")

names = ['theseus', 'egeus', 'lysander', 'demetreus', 'philostrate', 'quince', 'snug', 'bottom', 'flute',
        'snout', 'starveling','hippolyta', 'hermia', 'helena', 'oberon', 'titania', 'puck', 'peaseblossom',
        'cobweb','moth','mustardseed','pyramus','thisby','wall','moonshine','lion','duke', 'venice','prince',
        'morocco','arragon','antonio','bassanio','solanio','salerio','gratiano','lorenzo','shylock','tubal',
        'launcelot','gobbo','leonardo','balthasar','stephano','sirra','portia','nerissa','jessica','magnificoes',
        'gaoler','robin','goodfellow','solinus','aegeon','antipholus','syracuse','dromio','ephesus','balthazar',
        'angelo','merchant','pinch','aemilia','adriana','luciana','luce','courtezan','officer','hymen','hippolita',
        'emelia','nymphs','nymph','queen','knight','palamon','arcite','valerius','perithous','herald','gentleman',
        'messenger','wooer','keeper','jaylor','doctor','countreyman','countreymen','taborer','gerrold','frederick',
        'amiens','jaques','le','beau','charles','orlando','adam','dennis','touchstone','oliver','s','olivers','martext',
        'corin','silvius','william','hymen','rosalind','celia','phebe','audrey','forester','act','enter','exit',
        'exeunt','i','ii','iii','iv','v','electronic', 'version', 'complete', 'work', 'shakespeare', 'copyright',
        'world', 'library', 'inc', 'provided', 'project', 'gutenberg', 'etext', 'carnegie', 'mellon', 'university',
        'permission', 'electronic', 'machine', 'readable', 'copy', 'may', 'distributed', 'long', 'copy', 'others',
        'personal', 'use', 'distributed', 'used', 'commercially', 'prohibited', 'commercial', 'distribution',
        'includes', 'service', 'charge', 'download', 'membership','othello','desdemona','iago','emilia','cassio',
        'brabantio','gratiano','lodovico','roderigo','bianca','montano','clown','senator','senators','sailor','sailors'
        'escalus','paris','montague','capulet','romeo','mercutio','benvolio','tybalt','friar','lawrence','john','balthasar',
        'sampson','gregory','peter','abraham','apothecary','musician','musicians','chorus','page','juliet','nurse',
        'julius','caesar','octavius','mark','antony','lepidus','marcus','brutus','cassius','casca','trebonius','caius',
        'ligarius','decius','brutus','metellus','cimber','cinna','calpurnia','portia','cicero','popilius','lena','flavius',
        'marullus','tribune','cato','lucilius','titinius','messala','volumnius','artemidorus','cinna','varro','clitus',
        'claudio','strato','lucius','dardanius','pindarius','ghost','soothsayer','poet','claudius','marcellus','hamlet',
        'polonius','chamberlain','voltemand','cornelius','rosencrantz','guildenstern','osric','priest','bernardo','francisco',
        'reynaldo','player','players','fortinbras','norway','gertrude','ophelia','ber','fran','hor','mar','elsinore','king',
        'cor','laer','pol','ham','oph','rey','ros','guil','volt','lear','burgundy','cornwall','albany','kent','gloucester','glou',
        'alb','edgar','edmund','curan','fool','oswald','goneril','gon','regan','cordelia','edm','reg','cor','bur','france',
        'edg','osw','ti','th','u']
 
def allComedies():
    def As_You_Like_It():
        url = "http://www.gutenberg.org/files/1786/1786.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10672:149648]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Two_Noble_Kinsman():
        url = "http://www.gutenberg.org/files/1542/1542.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[13394:160639]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Comedy_Of_Errors():
        url = "http://www.gutenberg.org/files/1104/1104.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[11006:104661]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Merchant_Of_Venice():
        url = "http://www.gutenberg.org/files/1779/1779.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10889:144834]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def A_Midsummer_Nights_Dream():
        url = "http://www.gutenberg.org/files/1778/1778.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10755:120599]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    return As_You_Like_It() + ' ' + Two_Noble_Kinsman() + ' ' + Comedy_Of_Errors() + ' ' + Merchant_Of_Venice() + ' ' + A_Midsummer_Nights_Dream();


def allTragedies():
    def Othello():
        url = "http://www.gutenberg.org/files/1793/1793.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10510:187992]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Romeo_And_Juliet():
        url = "http://www.gutenberg.org/files/1513/1513.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[13961:159212]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Julius_Caesar():
        url = "http://www.gutenberg.org/files/1785/1785.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[11252:140870]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def Hamlet():
        url = "http://www.gutenberg.org/files/1787/1787.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10785:210063]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    def King_Lear():
        url = "http://www.gutenberg.org/files/1794/1794.txt"
        response = request.urlopen(url)
        raw = response.read().decode('utf8')
        raw = raw[10549:185333]
        raw = raw.translate(str.maketrans('','',string.punctuation))
        raw = raw.translate(str.maketrans('','',string.digits))
        raw.replace('\n', ' ')
        return raw;

    return Othello() + ' ' + Romeo_And_Juliet() + ' ' + Julius_Caesar() + ' ' + Hamlet() + ' ' + King_Lear()

allPlays = allComedies() + allTragedies()

def finalize(plays):
    wordnet = nltk.WordNetLemmatizer()
    stop = nltk.corpus.stopwords.words('english')
    plays = [i for i in word_tokenize(plays.lower()) if i not in stop]
    plays = ' '.join(plays)
    tagPlays = pos_tag(plays.split())
    playWords = [word for word,tag in tagPlays if tag != "NNP" and tag !="NNPS"] 
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(plays)
    update_tokens = []
    for i in range(len(tokens)):
        if tokens[i] in names:
            pass
        else:
            update_tokens.append(tokens[i])
    for i in range(len(update_tokens)):
        update_tokens[i] = wordnet.lemmatize(update_tokens[i])
    
    return update_tokens

def Most500(plays):
    fdist = FreqDist(plays)
    return fdist.most_common(500)

def wordExtraction(tokens):
    exList = []
    countList = []
    for i in range(len(tokens)):
        for j in range(len(tokens[i])):
            if isinstance(tokens[i][j], str):
                exList.append(tokens[i][j])
            else:
                countList.append(tokens[i][j])
    return exList;

def Most50Comedy(tokens):
    fdist = FreqDist(tokens)
    tokenList = wordExtraction(fdist.most_common(50));
    tokenList.append(1)
    return tokenList

def Most50Tragedy(tokens):
    fdist = FreqDist(tokens)
    tokenList = wordExtraction(fdist.most_common(50));
    tokenList.append(0)
    return tokenList

def findScene(tokens):
    sceneList = []
    for i in range(len(tokens)):
        if tokens[i] == 'scene':
            sceneList.append(i);
    return sceneList;
            


mostAllTokens = wordExtraction(Most500(finalize(allPlays)))

allComedyTokens = finalize(allComedies())
allTragedyTokens = finalize(allTragedies())

CScene1 = allComedyTokens[0:640]
CScene2 = allComedyTokens[640:1647]
CScene3 = allComedyTokens[1647:2141]
CScene4 = allComedyTokens[2141:2432]
CScene5 = allComedyTokens[2432:2509]
CScene6 = allComedyTokens[2509:2775]
CScene7 = allComedyTokens[2775:3154]
CScene8 = allComedyTokens[3154:3345]
CScene9 = allComedyTokens[3345:3436]
CScene10 = allComedyTokens[3436:3974]
CScene11 = allComedyTokens[3974:4084]
CScene12 = allComedyTokens[4084:4228]
CScene13 = allComedyTokens[4228:4306]
CScene14 = allComedyTokens[4306:5800]
CScene15 = allComedyTokens[5800:6171]
CScene16 = allComedyTokens[6171:6373]
CScene17 = allComedyTokens[6373:6920]
CScene18 = allComedyTokens[6920:7613]
CScene19 = allComedyTokens[7613:7686]
CScene20 = allComedyTokens[7686:8318]
CScene21 = allComedyTokens[8318:8542]
CScene22 = allComedyTokens[8542:8915]
CScene23 = allComedyTokens[8915:9068]
CScene24 = allComedyTokens[9068:15332]
CScene25 = allComedyTokens[15332:16423]
CScene26 = allComedyTokens[16423:22344]
CScene27 = allComedyTokens[22344:22927]
CScene28 = allComedyTokens[22927:23338]
CScene29 = allComedyTokens[23338:23825]
CScene30 = allComedyTokens[23825:24647]
CScene31 = allComedyTokens[24647:25264]
CScene32 = allComedyTokens[25264:25986]
CScene33 = allComedyTokens[25986:26416]
CScene34 = allComedyTokens[26416:26707]
CScene35 = allComedyTokens[26707:27067]
CScene36 = allComedyTokens[27067:27702]
CScene37 = allComedyTokens[27702:29297]
CScene38 = allComedyTokens[29297:29980]
CScene39 = allComedyTokens[29980:30476]
CScene40 = allComedyTokens[30476:31122]
CScene41 = allComedyTokens[31122:31315]
CScene42 = allComedyTokens[31315:32131]
CScene43 = allComedyTokens[32131:32217]
CScene44 = allComedyTokens[32217:32360]
CScene45 = allComedyTokens[32360:32567]
CScene46 = allComedyTokens[32567:32817]
CScene47 = allComedyTokens[32817:33141]
CScene48 = allComedyTokens[33141:33322]
CScene49 = allComedyTokens[33322:33725]
CScene50 = allComedyTokens[33725:34195]
CScene51 = allComedyTokens[34195:35397]

TScene1 = (allTragedyTokens[0:676])
TScene2 = allTragedyTokens[676:1057]
TScene3 = allTragedyTokens[1057:2589]
TScene4 = allTragedyTokens[2589:3827]
TScene5 = allTragedyTokens[3827:3884]
TScene6 = allTragedyTokens[3884:5287]
TScene7 = allTragedyTokens[5287:5496]
TScene8 = allTragedyTokens[5496:5523]
TScene9 = allTragedyTokens[5523:7355]
TScene10 = allTragedyTokens[7355:8047]
TScene11 = allTragedyTokens[8047:8995]
TScene12 = allTragedyTokens[8995:9898]
TScene13 = allTragedyTokens[9898:10313]
TScene14 = allTragedyTokens[10313:10806]
TScene15 = allTragedyTokens[10806:12262]
TScene16 = allTragedyTokens[12262:13182]
TScene17 = allTragedyTokens[13182:13610]
TScene18 = allTragedyTokens[13610:14077]
TScene19 = allTragedyTokens[14077:14540]
TScene20 = allTragedyTokens[14540:15203]
TScene21 = allTragedyTokens[15203:15375]
TScene22 = allTragedyTokens[15375:16191]
TScene23 = allTragedyTokens[16191:16630]
TScene24 = allTragedyTokens[16630:17401]
TScene25 = allTragedyTokens[17401:17725]
TScene26 = allTragedyTokens[17725:17876]
TScene27 = allTragedyTokens[17876:18690]
TScene28 = allTragedyTokens[18690:19282]
TScene29 = allTragedyTokens[19282:20046]
TScene30 = allTragedyTokens[20046:20187]
TScene31 = allTragedyTokens[20187:21178]
TScene32 = allTragedyTokens[21178:21704]
TScene33 = allTragedyTokens[21704:21890]
TScene34 = allTragedyTokens[21890:21969]
TScene35 = allTragedyTokens[21969:22114]
TScene36 = allTragedyTokens[22114:22253]
TScene37 = allTragedyTokens[22253:22826]
TScene38 = allTragedyTokens[22826:23177]
TScene39 = allTragedyTokens[23177:23280]
TScene40 = allTragedyTokens[23280:24566]
TScene41 = allTragedyTokens[24566:24884]
TScene42 = allTragedyTokens[24884:25942]
TScene43 = allTragedyTokens[25942:26529]
TScene44 = allTragedyTokens[26529:27694]
TScene45 = allTragedyTokens[27694:28172]
TScene46 = allTragedyTokens[28172:28229]
TScene47 = allTragedyTokens[28229:28432]
TScene48 = allTragedyTokens[28432:28858]
TScene49 = allTragedyTokens[28858:29553]
TScene50 = allTragedyTokens[29553:30580]
TScene51 = allTragedyTokens[30580:30730]
TScene52 = allTragedyTokens[30730:30908]
TScene53 = allTragedyTokens[30908:31102]
TScene54 = allTragedyTokens[31102:32215]
TScene55 = allTragedyTokens[32215:32682]
TScene56 = allTragedyTokens[32682:32711]
TScene57 = allTragedyTokens[32711:33161]
TScene58 = allTragedyTokens[33161:33289]
TScene59 = allTragedyTokens[33289:33597]
TScene60 = allTragedyTokens[33597:34236]
TScene61 = allTragedyTokens[34236:35232]
TScene62 = allTragedyTokens[35232:35736]
TScene63 = allTragedyTokens[35736:36093]
TScene64 = allTragedyTokens[36093:36846]
TScene65 = allTragedyTokens[36846:37250]
TScene66 = allTragedyTokens[37250:38621]
TScene67 = allTragedyTokens[38621:38776]
TScene68 = allTragedyTokens[38776:39369]
TScene69 = allTragedyTokens[39369:39429]
TScene70 = allTragedyTokens[39429:40128]
TScene71 = allTragedyTokens[40128:40420]
TScene72 = allTragedyTokens[40420:41634]
TScene73 = allTragedyTokens[41634:42020]
TScene74 = allTragedyTokens[42020:42890]
TScene75 = allTragedyTokens[42890:43068]
TScene76 = allTragedyTokens[43068:43159]
TScene77 = allTragedyTokens[43159:43433]
TScene78 = allTragedyTokens[43433:43696]
TScene79 = allTragedyTokens[43696:44506]
TScene80 = allTragedyTokens[44506:44643]
TScene81 = allTragedyTokens[44643:45354]
TScene82 = allTragedyTokens[45354:46465]
TScene83 = allTragedyTokens[46465:48018]
TScene84 = allTragedyTokens[48018:49192]
TScene85 = allTragedyTokens[49192:49888]
TScene86 = allTragedyTokens[49888:50002]
TScene87 = allTragedyTokens[50002:51295]
TScene88 = allTragedyTokens[51295:51473]
TScene89 = allTragedyTokens[51473:51992]
TScene90 = allTragedyTokens[51992:52691]
TScene91 = allTragedyTokens[52691:52782]
TScene92 = allTragedyTokens[52782:53994]
TScene93 = allTragedyTokens[53994:54192]
TScene94 = allTragedyTokens[54192:54588]
TScene95 = allTragedyTokens[54588:54688]
TScene96 = allTragedyTokens[54688:55507]
TScene97 = allTragedyTokens[55507:55598]
TScene98 = allTragedyTokens[55598:56074]
TScene99 = allTragedyTokens[56074:56567]
TScene100 = allTragedyTokens[56567:56939]
TScene101 = allTragedyTokens[56939:57333]
TScene102 = allTragedyTokens[57333:57554]
TScene103 = allTragedyTokens[57554:57680]
TScene104 = allTragedyTokens[57680:57827]
TScene105 = allTragedyTokens[57827:59093]
TScene106 = allTragedyTokens[59093:59483]
TScene107 = allTragedyTokens[59483:59744]
TScene108 = allTragedyTokens[59744:59809]

CSceneList = [CScene1, CScene2, CScene3, CScene4, CScene5,
              CScene6, CScene7, CScene8, CScene9, CScene10,
              CScene11, CScene12, CScene13, CScene14, CScene15,
              CScene16, CScene17, CScene18, CScene19, CScene20,
              CScene21, CScene22, CScene23, CScene24, CScene25,
              CScene26, CScene27, CScene28, CScene29, CScene30,
              CScene31, CScene32, CScene33, CScene34, CScene35,
              CScene36, CScene37, CScene38, CScene39, CScene40,
              CScene41, CScene42, CScene43, CScene44, CScene45,
              CScene46, CScene47, CScene48, CScene49, CScene50,
              CScene51]

TSceneList = [TScene1, TScene2, TScene3, TScene4, TScene5, TScene6,
              TScene7, TScene8, TScene9, TScene10, TScene11, TScene12,
              TScene13, TScene14, TScene15, TScene16, TScene17, TScene18,
              TScene19, TScene20, TScene21, TScene22, TScene23, TScene24,
              TScene25, TScene26, TScene27, TScene28, TScene29,TScene30,
              TScene31, TScene32, TScene33, TScene34, TScene35, TScene36,
              TScene37, TScene38, TScene39, TScene40, TScene41, TScene42,
              TScene43, TScene44, TScene45, TScene46, TScene47, TScene48,
              TScene49, TScene50, TScene51, TScene52, TScene53, TScene54,
              TScene55, TScene56, TScene57, TScene58, TScene59, TScene60,
              TScene61, TScene62, TScene63, TScene64, TScene65, TScene66,
              TScene67, TScene68, TScene69, TScene70, TScene71, TScene72,
              TScene73, TScene74, TScene75, TScene76, TScene77, TScene78,
              TScene79, TScene80, TScene81, TScene82, TScene83, TScene84,
              TScene85, TScene86, TScene87, TScene88, TScene89, TScene90,
              TScene91, TScene92, TScene93, TScene94, TScene95, TScene96,
              TScene97, TScene98, TScene99, TScene100, TScene101, TScene102,
              TScene103, TScene104, TScene105, TScene106, TScene107,
              TScene108]

for i in range(len(CSceneList)):
    CSceneList[i].append('comedy'+str(i+1))
for i in range(len(TSceneList)):
    TSceneList[i].append('tragedy'+str(i+1))

ASceneList = CSceneList + TSceneList
'''def makeAllSceneList(list1, list2, list3):
    for i in list1:
        list3.append(list1[i])
    for i in list2:
        list3.append(list2[i])

makeAllSceneList(CSceneList, TSceneList, ASceneList)'''

def searchScene(scene, word):
    count = 0
    for i in range(len(scene)):
        if scene[i] == word:
            count += 1
    return str(count)

def searchCSceneList(wordList, sceneList):
    for i in range(len(sceneList)):
        countList=[]
        for j in range(len(wordList)):
            wordcount = searchScene(sceneList[i], wordList[j])
            countList.append(wordcount)
        countList.append('comedy\n')
        BOWfile.write(", ".join(countList))

def searchTSceneList(wordList, sceneList):
    for i in range(len(sceneList)):
        countList=[]
        for j in range(len(wordList)):
            wordcount = searchScene(sceneList[i], wordList[j])
            countList.append(wordcount)
        countList.append('tragedy\n')
        BOWfile.write(", ".join(countList))

def formatWords(wordList):
    BOWfile.write('@RELATION shakespeare' + '\n' + '\n')
    for i in range(len(wordList)):
        BOWfile.write('@ATTRIBUTE ' + wordList[i] + ' REAL' + '\n')
    BOWfile.write('@ATTRIBUTE ' + 'class' + ' {comedy, tragedy}' + '\n')

formatWords(mostAllTokens)
BOWfile.write('\n' + '@DATA' + '\n')
searchCSceneList(mostAllTokens, CSceneList)
searchTSceneList(mostAllTokens, TSceneList)

BOWfile.close()

#print(searchScene(TScene1, 'beauty'))
